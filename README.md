# Movinradio Backend

## Prerequisites

Have [Docker](https://www.docker.com), [Maven 3.8+](https://maven.apache.org) and [Java 17+](https://adoptium.net) installed

Copy `./.env.dev` to `./.env`

## Development

Start development infrastructure (database) with:

```bash
docker compose up -d
# or
docker-compose up -d
```

Run the project with:

```bash
mvn clean spring-boot:run
```

Stop development infrastructure (database) with:

```bash
docker compose down
# or
docker-compose down
```

Run tests with:

```bash
mvn clean verify -U
```
