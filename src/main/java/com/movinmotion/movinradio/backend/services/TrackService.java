package com.movinmotion.movinradio.backend.services;

import java.util.List;

import com.movinmotion.movinradio.backend.entities.Track;

public interface TrackService {
  /**
   * Get all tracks from a provider
   * 
   * @param providerId Provider ID
   * @return Tracks from provider
   */
  List<Track> getTracksFromProvider(String providerId);

  /**
   * Get a list of persisted tracks from a list of tracks, if any of the tracks in
   * the given list is not persisted, a persisted clone is created
   * 
   * @param tracks Tracks
   * @return Persisted tracks
   */
  List<Track> getPersistedTracks(List<Track> tracks);

  /**
   * Delete a track if it is no longer used by any playlist
   * 
   * @param track Track
   */
  void deleteTrackIfOrphan(Track track);
}
