package com.movinmotion.movinradio.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.movinmotion.movinradio.backend.entities.Track;
import com.movinmotion.movinradio.backend.enums.TrackProvider;

@Service
public class TrackProviderInternalServiceImpl implements TrackProviderInternalService {

  private static class TrackDescription {
    final String title;
    final String artist;
    final String album;

    TrackDescription(String title, String artist, String album) {
      this.title = title;
      this.artist = artist;
      this.album = album;
    }
  }

  private List<Track> allTracks = new ArrayList<>();

  public TrackProviderInternalServiceImpl() {
    List<TrackDescription> allTrackDescriptions = new ArrayList<>();
    allTrackDescriptions.add(new TrackDescription("Smells Like a Teen Spirit", "Nirvana", "Nevermind"));
    allTrackDescriptions.add(new TrackDescription("Like a Rolling Stone", "Bob Dylan", "Highway 61 Revisited"));
    allTrackDescriptions.add(new TrackDescription("Imagine", "John Lennon", "Imagine"));
    allTrackDescriptions.add(new TrackDescription("One", "U2", "Achtung Baby"));
    allTrackDescriptions.add(new TrackDescription("Billie Jean", "Michael Jackson", "Thriller"));
    allTrackDescriptions.add(new TrackDescription("Hey Jude", "Beatles", "Hey Jude"));
    allTrackDescriptions
        .add(new TrackDescription("(I Can’t Get No) Satisfaction", "Rolling stones", "Out of Our Heads"));
    allTrackDescriptions.add(new TrackDescription("Bohemian Rhapsody", "Queen", "A Night at the Opera"));
    allTrackDescriptions.add(new TrackDescription("Crazy in love", "Beyoncé et Jay Z", "Dangerously in Love"));
    allTrackDescriptions.add(new TrackDescription("God Saves The Queen", "The Sex Pistols",
        "Never Mind the Bollocks, Here's the Sex Pistols"));
    allTrackDescriptions.add(new TrackDescription("What’s Going On", "Marvin Gaye", "What's Going On"));
    allTrackDescriptions.add(new TrackDescription("Clocks", "ColdPlay", "A Rush of Blood to the Head"));
    allTrackDescriptions.add(new TrackDescription("Respect", "Aretha Franklin", "30 Greatest Hits"));
    allTrackDescriptions.add(new TrackDescription("Johny B. Goode", "Chuck Berry", "Chuck Berry Is on Top"));
    allTrackDescriptions.add(new TrackDescription("Stairway to Heaven", "Led Zeppelin", "Led Zeppelin IV"));
    allTrackDescriptions.add(new TrackDescription("What’d I Say", "Ray Charles", "What'd I Say"));
    allTrackDescriptions.add(new TrackDescription("London Calling", "The Clash", "London Calling"));
    allTrackDescriptions.add(new TrackDescription("Wish You Were Here", "Pink Floyd", "Wish You Were Here"));
    allTrackDescriptions.add(new TrackDescription("Your Song", "Elton John", "Elton John"));
    allTrackDescriptions.add(new TrackDescription("Cry Me a River", "Justin Timberlake", "Justified"));
    for (int i = 0; i < allTrackDescriptions.size(); i++) {
      this.allTracks.add(new Track(TrackProvider.INTERNAL, String.valueOf(i), allTrackDescriptions.get(i).title,
          allTrackDescriptions.get(i).artist, allTrackDescriptions.get(i).album));
    }
  }

  @Override
  public List<Track> getAllTracks() {
    return allTracks;
  }

}
