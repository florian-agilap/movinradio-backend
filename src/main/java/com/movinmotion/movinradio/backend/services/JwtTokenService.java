package com.movinmotion.movinradio.backend.services;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtTokenService {

  /**
   * Generate a JWT token from user details
   * 
   * @param userDetails User details
   * @return JWT token
   */
  String generateJwtToken(UserDetails userDetails);

  /**
   * Get user details from JWT token
   * 
   * @param token JWT token
   * @return User details
   */
  Optional<UserDetails> getUserDetailsFromToken(String token);
}
