package com.movinmotion.movinradio.backend.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.movinmotion.movinradio.backend.entities.Playlist;
import com.movinmotion.movinradio.backend.entities.Track;
import com.movinmotion.movinradio.backend.exceptions.BadRequestException;
import com.movinmotion.movinradio.backend.exceptions.InternalServerErrorException;
import com.movinmotion.movinradio.backend.exceptions.NotFoundException;
import com.movinmotion.movinradio.backend.repositories.PlaylistRepository;

@Service
public class PlaylistServiceImpl implements PlaylistService {

  @Autowired
  private TrackService trackService;

  @Autowired
  private PlaylistRepository playlistRepository;

  @Override
  public List<Playlist> getPlaylists(String owner) {
    return playlistRepository.findByOwner(owner);
  }

  @Transactional
  @Override
  public Playlist createPlaylist(Playlist playlist) {
    playlist.setTracks(trackService.getPersistedTracks(playlist.getTracks()));
    return playlistRepository.save(playlist);
  }

  @Transactional
  @Override
  public Playlist updatePlaylist(Long id, Playlist playlistDetails) {
    Playlist playlist = playlistRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(Playlist.class, id));

    List<Track> originalTracks = new ArrayList<>(playlist.getTracks());

    playlist.setOwner(playlistDetails.getOwner());
    playlist.setName(playlistDetails.getName());
    playlist.setTracks(trackService.getPersistedTracks(playlistDetails.getTracks()));

    playlist = playlistRepository.save(playlist);

    for (Track originalTrack : originalTracks) {
      trackService.deleteTrackIfOrphan(originalTrack);
    }

    return playlist;
  }

  @Transactional
  @Override
  public void deletePlaylist(Long id) {
    Playlist playlist = playlistRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(Playlist.class, id));

    List<Track> originalTracks = new ArrayList<>(playlist.getTracks());

    playlistRepository.delete(playlist);

    for (Track originalTrack : originalTracks) {
      trackService.deleteTrackIfOrphan(originalTrack);
    }
  }

  /**
   * Add data of a playlist to a PDF document
   * 
   * @param document PDF document
   * @param playlist Playlist to add data from
   */
  private void addPlaylistContentToPdf(Document document, Playlist playlist) {
    document.add(new Paragraph(playlist.getName()));

    PdfPTable table = new PdfPTable(4);
    table.setSpacingBefore(20);
    table.setSpacingAfter(20);

    Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

    PdfPCell hcell;
    hcell = new PdfPCell(new Phrase("Title", headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("Artist", headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("Album", headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    hcell = new PdfPCell(new Phrase("Provider", headFont));
    hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table.addCell(hcell);

    for (Track track : playlist.getTracks()) {
      PdfPCell cell;

      cell = new PdfPCell(new Phrase(track.getTitle()));
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_LEFT);
      table.addCell(cell);

      cell = new PdfPCell(new Phrase(track.getArtist()));
      cell.setPaddingLeft(5);
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_LEFT);
      table.addCell(cell);

      cell = new PdfPCell(new Phrase(track.getAlbum()));
      cell.setPaddingLeft(5);
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_LEFT);
      table.addCell(cell);

      cell = new PdfPCell(new Phrase(track.getProvider().name()));
      cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
      cell.setHorizontalAlignment(Element.ALIGN_LEFT);
      cell.setPaddingRight(5);
      table.addCell(cell);
    }

    document.add(table);
  }

  public InputStream getPlaylistPDFExport(Long id) {
    Playlist playlist = playlistRepository.findById(id)
        .orElseThrow(() -> new NotFoundException(Playlist.class, id));
    try (Document document = new Document(); ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      PdfWriter.getInstance(document, outputStream);
      document.open();
      addPlaylistContentToPdf(document, playlist);
      document.close();
      return new ByteArrayInputStream(outputStream.toByteArray());
    } catch (Exception e) {
      throw new InternalServerErrorException("Failed to generate PDF export of playlists", e);
    }
  }

  public InputStream getPlaylistsPDFExport(String owner) {
    List<Playlist> playlists = playlistRepository.findByOwner(owner);
    if (playlists.isEmpty()) {
      throw new BadRequestException("Can't export playlists as there is no playlist for current user");
    }
    try (Document document = new Document(); ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
      PdfWriter.getInstance(document, outputStream);
      document.open();
      for (Playlist playlist : playlists) {
        addPlaylistContentToPdf(document, playlist);
      }
      document.close();
      return new ByteArrayInputStream(outputStream.toByteArray());
    } catch (Exception e) {
      throw new InternalServerErrorException("Failed to generate PDF export of playlist", e);
    }
  }

}
