package com.movinmotion.movinradio.backend.services;

import java.util.List;

import com.movinmotion.movinradio.backend.entities.Track;

public interface TrackProviderService {
  List<Track> getAllTracks();
}
