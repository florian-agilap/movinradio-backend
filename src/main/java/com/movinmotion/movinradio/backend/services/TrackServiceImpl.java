package com.movinmotion.movinradio.backend.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.movinmotion.movinradio.backend.entities.Track;
import com.movinmotion.movinradio.backend.enums.TrackProvider;
import com.movinmotion.movinradio.backend.exceptions.BadRequestException;
import com.movinmotion.movinradio.backend.repositories.PlaylistRepository;
import com.movinmotion.movinradio.backend.repositories.TrackRepository;

@Service
public class TrackServiceImpl implements TrackService {

  @Autowired
  private TrackRepository trackRepository;

  @Autowired
  private PlaylistRepository playlistRepository;

  @Autowired
  private TrackProviderInternalService trackProviderInternalService;

  @Override
  public List<Track> getTracksFromProvider(String provider) {
    TrackProvider trackProvider = TrackProvider.valueOf(provider);
    switch (trackProvider) {
      case INTERNAL:
        return trackProviderInternalService.getAllTracks();
      default:
        throw new BadRequestException("Unknown track provider \"" + trackProvider + "\"");
    }
  }

  @Transactional
  @Override
  public List<Track> getPersistedTracks(List<Track> tracks) {
    List<Track> persistedTracks = new ArrayList<>();
    for (Track track : tracks) {
      if (track.getId() == null) {
        track = new Track(track);
        track = trackRepository.save(track);
      }
      persistedTracks.add(track);
    }
    return persistedTracks;
  }

  @Transactional
  @Override
  public void deleteTrackIfOrphan(Track track) {
    if (playlistRepository.countByTracks(track) == 0) {
      trackRepository.delete(track);
    }
  }

}
