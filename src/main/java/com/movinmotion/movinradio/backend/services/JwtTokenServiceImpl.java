package com.movinmotion.movinradio.backend.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.movinmotion.movinradio.backend.models.ApiUser;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JwtTokenServiceImpl implements JwtTokenService {

  @Value("${jwt.secret}")
  private String jwtSecret;

  public String generateJwtToken(UserDetails userDetails) {
    Map<String, Object> claims = new HashMap<>();
    claims.put("Authorities", userDetails.getAuthorities());

    return Jwts.builder().setClaims(claims).setSubject(userDetails.getUsername())
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + 7 * 24 * 3600 * 1000))
        .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
  }

  public Optional<UserDetails> getUserDetailsFromToken(String token) {

    final Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();

    boolean valid = !claims.getExpiration().before(new Date());

    if (!valid) {
      return Optional.empty();
    }

    String username = claims.getSubject();

    @SuppressWarnings("unchecked")
    Collection<LinkedHashMap<String, String>> claimMap = claims.get("Authorities", Collection.class);
    Collection<String> authorities = claimMap.stream()
        .map(x -> x.values()
            .stream()
            .findFirst()
            .orElse(""))
        .collect(Collectors.toList());

    return Optional.of(new ApiUser(username, authorities));
  }
}
