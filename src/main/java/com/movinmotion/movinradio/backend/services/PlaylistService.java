package com.movinmotion.movinradio.backend.services;

import java.io.InputStream;
import java.util.List;

import com.movinmotion.movinradio.backend.entities.Playlist;

public interface PlaylistService {
  /**
   * Get all playlists from an owner
   * 
   * @param owner Owner
   * @return Owner's playlists
   */
  List<Playlist> getPlaylists(String owner);

  /**
   * Create a playlist
   * 
   * @param playlist Playlist to create
   * @return Created playlist
   */
  Playlist createPlaylist(Playlist playlist);

  /**
   * Update a playlist
   * 
   * @param id              Id of the playlist to update
   * @param playlistDetails Details to update
   * @return Updated playlist
   */
  Playlist updatePlaylist(Long id, Playlist playlistDetails);

  /**
   * Delete a playlist
   * 
   * @param id Id of the playlist to delete
   */
  void deletePlaylist(Long id);

  /**
   * Get PDF export of a given playlist
   * 
   * @param id Id of the playlist to export
   * @return PDF export inputstream
   */
  InputStream getPlaylistPDFExport(Long id);

  /**
   * Get PDF export of all playlists of a user
   * 
   * @param owner Owner of the playlists
   * @return PDF export inputstream
   */
  InputStream getPlaylistsPDFExport(String owner);
}
