package com.movinmotion.movinradio.backend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Helper exception class to respond with a 404 status code
 */
public class NotFoundException extends ResponseStatusException {

  public NotFoundException(String message) {
    super(HttpStatus.NOT_FOUND, message);
  }

  public NotFoundException(String message, Throwable cause) {
    super(HttpStatus.NOT_FOUND, message, cause);
  }

  public NotFoundException(Class<?> clazz, long id) {
    this(String.format("Entity %s with id %d not found", clazz.getSimpleName(), id));
  }

  public NotFoundException(Class<?> clazz, long id, Throwable cause) {
    this(String.format("Entity %s with id %d not found", clazz.getSimpleName(), id), cause);
  }

}
