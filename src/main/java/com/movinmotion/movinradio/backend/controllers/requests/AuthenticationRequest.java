package com.movinmotion.movinradio.backend.controllers.requests;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AuthenticationRequest implements Serializable {

  private static final long serialVersionUID = 4726499563705156521L;

  @NotNull
  @Size(max = 255)
  private String username;

  @NotNull
  @Size(max = 255)
  private String password;

  public AuthenticationRequest() {
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
