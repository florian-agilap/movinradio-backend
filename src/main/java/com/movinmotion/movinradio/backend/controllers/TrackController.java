package com.movinmotion.movinradio.backend.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movinmotion.movinradio.backend.entities.Track;
import com.movinmotion.movinradio.backend.enums.TrackProvider;
import com.movinmotion.movinradio.backend.services.TrackService;

@RestController
@RequestMapping(path = "/api/v1/tracks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TrackController {

  @Autowired
  private TrackService trackService;

  @GetMapping("/providers")
  public ResponseEntity<List<TrackProvider>> getTrackProviders() {
    return ResponseEntity.ok(Arrays.asList(TrackProvider.values()));
  }

  @GetMapping("/{provider}")
  public ResponseEntity<List<Track>> getTracksFromProvider(@PathVariable String provider) {
    return ResponseEntity.ok(trackService.getTracksFromProvider(provider));
  }
}
