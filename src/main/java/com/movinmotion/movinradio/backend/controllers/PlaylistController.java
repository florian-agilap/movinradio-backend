package com.movinmotion.movinradio.backend.controllers;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movinmotion.movinradio.backend.entities.Playlist;
import com.movinmotion.movinradio.backend.exceptions.BadRequestException;
import com.movinmotion.movinradio.backend.services.PlaylistService;

@RestController
@RequestMapping(path = "/api/v1/playlists", produces = MediaType.APPLICATION_JSON_VALUE)
public class PlaylistController {

  @Autowired
  private PlaylistService playlistService;

  private String getCurrentUsername() {
    return SecurityContextHolder.getContext().getAuthentication().getName();
  }

  @GetMapping
  public ResponseEntity<List<Playlist>> getPlaylists() {
    return ResponseEntity.ok(playlistService.getPlaylists(this.getCurrentUsername()));
  }

  @PostMapping
  public ResponseEntity<Playlist> createPlaylist(@RequestBody Playlist playlist) {
    if (!StringUtils.equals(playlist.getOwner(), this.getCurrentUsername())) {
      throw new BadRequestException("Current user is not allowed to create a playlist for the given user");
    }
    return ResponseEntity.ok(playlistService.createPlaylist(playlist));
  }

  @PutMapping("/{id}")
  public ResponseEntity<Playlist> updatePlaylist(@PathVariable Long id, @RequestBody Playlist playlistDetails) {
    if (!StringUtils.equals(playlistDetails.getOwner(), this.getCurrentUsername())) {
      throw new BadRequestException("Current user is not allowed to update a playlist for the given user");
    }
    return ResponseEntity.ok(playlistService.updatePlaylist(id, playlistDetails));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Map<String, Boolean>> deletePlaylist(@PathVariable Long id) {
    playlistService.deletePlaylist(id);
    Map<String, Boolean> response = new HashMap<>();
    response.put("deleted", Boolean.TRUE);
    return ResponseEntity.ok(response);
  }

  private ResponseEntity<InputStreamResource> generatePdfResponse(InputStream inputStream) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentDisposition(ContentDisposition.builder("attachment")
        .filename("playlists-export.pdf")
        .build());

    return ResponseEntity
        .ok()
        .headers(headers)
        .contentType(MediaType.APPLICATION_PDF)
        .body(new InputStreamResource(inputStream));
  }

  @GetMapping(value = "/export", produces = MediaType.APPLICATION_PDF_VALUE)
  public ResponseEntity<InputStreamResource> getPlaylistsPDFExport() {
    return this.generatePdfResponse(playlistService.getPlaylistsPDFExport(this.getCurrentUsername()));
  }

  @GetMapping(value = "/{id}/export", produces = MediaType.APPLICATION_PDF_VALUE)
  public ResponseEntity<InputStreamResource> getPlaylistPDFExport(@PathVariable Long id) {
    return this.generatePdfResponse(playlistService.getPlaylistPDFExport(id));
  }
}
