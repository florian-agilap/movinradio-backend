package com.movinmotion.movinradio.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movinmotion.movinradio.backend.controllers.requests.AuthenticationRequest;
import com.movinmotion.movinradio.backend.controllers.responses.AuthenticationResponse;
import com.movinmotion.movinradio.backend.exceptions.NotFoundException;
import com.movinmotion.movinradio.backend.repositories.UserRepository;
import com.movinmotion.movinradio.backend.services.JwtTokenService;

@RestController
@RequestMapping(path = "/api/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController {

  @Autowired
  private JwtTokenService jwtTokenService;

  @Autowired
  private UserRepository userRepository;

  @PostMapping("/authenticate")
  public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
    UserDetails userDetails = userRepository
        .findByUsernameAndPassword(authenticationRequest.getUsername(), authenticationRequest.getPassword())
        .orElseThrow(() -> new NotFoundException("The username/password did not match"));
    String jwtToken = jwtTokenService.generateJwtToken(userDetails);
    return ResponseEntity.ok(new AuthenticationResponse(jwtToken));
  }
}
