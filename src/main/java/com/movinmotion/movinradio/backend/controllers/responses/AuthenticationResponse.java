package com.movinmotion.movinradio.backend.controllers.responses;

import java.io.Serializable;

public class AuthenticationResponse implements Serializable {

  private static final long serialVersionUID = 4726499563705156521L;

  private String accessToken;

  public AuthenticationResponse() {
  }

  public AuthenticationResponse(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getAccessToken() {
    return this.accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

}
