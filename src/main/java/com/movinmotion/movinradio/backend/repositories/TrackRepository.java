package com.movinmotion.movinradio.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movinmotion.movinradio.backend.entities.Track;

@Repository
public interface TrackRepository extends JpaRepository<Track, Long> {

}
