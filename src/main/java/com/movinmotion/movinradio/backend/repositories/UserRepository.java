package com.movinmotion.movinradio.backend.repositories;

import org.springframework.stereotype.Component;

import com.movinmotion.movinradio.backend.models.ApiUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserRepository {

  private static final List<ApiUser> users = new ArrayList<>();

  static {
    for (int i = 1; i <= 3; i++) {
      users.add(new ApiUser("user" + i, "test", List.of()));
    }
  }

  public Optional<ApiUser> findByUsernameAndPassword(String username, String password) {
    return users.stream()
        .filter(x -> x.getUsername().equals(username) && x.getPassword().equals(password))
        .findFirst();
  }
}
