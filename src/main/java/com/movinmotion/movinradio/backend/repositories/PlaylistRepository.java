package com.movinmotion.movinradio.backend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movinmotion.movinradio.backend.entities.Playlist;
import com.movinmotion.movinradio.backend.entities.Track;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Long> {

  List<Playlist> findByOwner(String owner);

  long countByTracks(Track track);

}
