package com.movinmotion.movinradio.backend.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.movinmotion.movinradio.backend.enums.TrackProvider;

@Entity
@Table(name = "track")
public class Track {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Enumerated(EnumType.STRING)
  private TrackProvider provider;

  @NotNull
  @Size(max = 255)
  @Column(name = "track_provider_ref")
  private String trackProviderRef;

  @NotNull
  @Size(max = 255)
  @Column(name = "title")
  private String title;

  @NotNull
  @Size(max = 255)
  @Column(name = "artist")
  private String artist;

  @NotNull
  @Size(max = 255)
  @Column(name = "album")
  private String album;

  public Track() {
  }

  public Track(TrackProvider provider, String trackProviderRef, String title, String artist, String album) {
    this.provider = provider;
    this.trackProviderRef = trackProviderRef;
    this.title = title;
    this.artist = artist;
    this.album = album;
  }

  public Track(Track track) {
    this.provider = track.getProvider();
    this.trackProviderRef = track.getTrackProviderRef();
    this.title = track.getTitle();
    this.artist = track.getArtist();
    this.album = track.getAlbum();
  }

  public TrackProvider getProvider() {
    return this.provider;
  }

  public void setProvider(TrackProvider provider) {
    this.provider = provider;
  }

  public String getTrackProviderRef() {
    return this.trackProviderRef;
  }

  public void setTrackProviderRef(String trackProviderRef) {
    this.trackProviderRef = trackProviderRef;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getArtist() {
    return this.artist;
  }

  public void setArtist(String artist) {
    this.artist = artist;
  }

  public String getAlbum() {
    return this.album;
  }

  public void setAlbum(String album) {
    this.album = album;
  }

}
