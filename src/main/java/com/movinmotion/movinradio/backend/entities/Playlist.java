package com.movinmotion.movinradio.backend.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "playlist")
public class Playlist {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Size(max = 255)
  @Column(name = "owner")
  private String owner;

  @NotNull
  @Size(max = 255)
  @Column(name = "name")
  private String name;

  @NotNull
  @OneToMany(fetch = FetchType.EAGER)
  private List<Track> tracks = new ArrayList<>();

  public Playlist() {
  }

  public Playlist(String owner, String name, List<Track> tracks) {
    this.owner = owner;
    this.name = name;
    this.tracks = tracks;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOwner() {
    return this.owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Track> getTracks() {
    return this.tracks;
  }

  public void setTracks(List<Track> tracks) {
    this.tracks = tracks;
  }

}
