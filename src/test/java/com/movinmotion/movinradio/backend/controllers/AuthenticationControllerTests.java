package com.movinmotion.movinradio.backend.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.movinmotion.movinradio.backend.controllers.requests.AuthenticationRequest;
import com.movinmotion.movinradio.backend.controllers.responses.AuthenticationResponse;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AuthenticationControllerTests extends ControllersTest {

  @Test
  public void authenticateShouldReturnToken() throws Exception {
    AuthenticationRequest request = new AuthenticationRequest();
    request.setUsername("user1");
    request.setPassword("test");
    ResponseEntity<AuthenticationResponse> response = postWithoutAuth("/auth/authenticate", request,
        AuthenticationResponse.class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody().getAccessToken()).isNotEmpty();
  }

  @Test
  public void authenticateWithWrongUserShouldReturnError() throws Exception {
    AuthenticationRequest request = new AuthenticationRequest();
    request.setUsername("user4");
    request.setPassword("test");
    ResponseEntity<AuthenticationResponse> response = postWithoutAuth("/auth/authenticate", request,
        AuthenticationResponse.class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

}
