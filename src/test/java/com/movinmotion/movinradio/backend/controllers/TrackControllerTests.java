package com.movinmotion.movinradio.backend.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.movinmotion.movinradio.backend.entities.Track;
import com.movinmotion.movinradio.backend.enums.TrackProvider;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class TrackControllerTests extends ControllersTest {

  @Test
  public void getTrackProvidersShouldReturnProvidersList() throws Exception {
    ResponseEntity<String[]> response = getWithAuth("/tracks/providers", String[].class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isNotEmpty();
    assertThat(response.getBody().length).isEqualTo(1);
    assertThat(response.getBody()[0]).isEqualTo(TrackProvider.INTERNAL.name());
  }

  @Test
  public void getTrackProvidersWithoutAuthenticationShouldReturnError() throws Exception {
    ResponseEntity<String[]> response = getWithoutAuth("/tracks/providers", String[].class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
  }

  @Test
  public void getTracksFromProviderShouldReturnTracksList() throws Exception {
    ResponseEntity<Track[]> response = getWithAuth("/tracks/" + TrackProvider.INTERNAL.name(), Track[].class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isNotEmpty();
    assertThat(response.getBody().length).isEqualTo(20);
  }

  @Test
  public void getTracksFromProviderWithoutAuthenticationShouldReturnError() throws Exception {
    ResponseEntity<Track[]> response = getWithoutAuth("/tracks/" + TrackProvider.INTERNAL.name(), Track[].class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
  }

}
