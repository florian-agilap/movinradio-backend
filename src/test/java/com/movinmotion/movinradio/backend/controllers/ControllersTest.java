package com.movinmotion.movinradio.backend.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.movinmotion.movinradio.backend.controllers.requests.AuthenticationRequest;
import com.movinmotion.movinradio.backend.controllers.responses.AuthenticationResponse;

public abstract class ControllersTest {

  @LocalServerPort
  private int port;

  @Autowired
  protected TestRestTemplate restTemplate;

  protected HttpHeaders authHeaders;

  @BeforeEach
  public void initEach() {
    AuthenticationRequest request = new AuthenticationRequest();
    request.setUsername("user1");
    request.setPassword("test");
    AuthenticationResponse response = this.restTemplate.postForObject(
        getApiUrl() + "/auth/authenticate", request,
        AuthenticationResponse.class);
    this.authHeaders = new HttpHeaders();
    this.authHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + response.getAccessToken());
  }

  protected String getApiUrl() {
    return "http://localhost:" + port + "/api/v1";
  }

  protected <T> ResponseEntity<T> getWithAuth(String relativeApiUrl, Class<T> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.GET,
        new HttpEntity<>(this.authHeaders), responseType);
  }

  protected <T> ResponseEntity<T> getWithoutAuth(String relativeApiUrl, Class<T> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.GET,
        new HttpEntity<>(new HttpHeaders()), responseType);
  }

  protected <T1, T2> ResponseEntity<T2> postWithAuth(String relativeApiUrl, T1 body, Class<T2> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.POST,
        new HttpEntity<>(body, this.authHeaders), responseType);
  }

  protected <T1, T2> ResponseEntity<T2> postWithoutAuth(String relativeApiUrl, T1 body, Class<T2> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.POST,
        new HttpEntity<>(body, new HttpHeaders()), responseType);
  }

  protected <T1, T2> ResponseEntity<T2> putWithAuth(String relativeApiUrl, T1 body, Class<T2> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.PUT,
        new HttpEntity<>(body, this.authHeaders), responseType);
  }

  protected <T1, T2> ResponseEntity<T2> putWithoutAuth(String relativeApiUrl, T1 body, Class<T2> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.PUT,
        new HttpEntity<>(body, new HttpHeaders()), responseType);
  }

  protected <T> ResponseEntity<T> deleteWithAuth(String relativeApiUrl, Class<T> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.PUT,
        new HttpEntity<>(this.authHeaders), responseType);
  }

  protected <T> ResponseEntity<T> deleteWithoutAuth(String relativeApiUrl, Class<T> responseType) {
    return this.restTemplate.exchange(getApiUrl() + relativeApiUrl, HttpMethod.PUT,
        new HttpEntity<>(new HttpHeaders()), responseType);
  }

}
