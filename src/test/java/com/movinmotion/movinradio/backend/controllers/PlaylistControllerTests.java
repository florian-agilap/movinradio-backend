package com.movinmotion.movinradio.backend.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.movinmotion.movinradio.backend.entities.Playlist;
import com.movinmotion.movinradio.backend.repositories.PlaylistRepository;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class PlaylistControllerTests extends ControllersTest {

  @Autowired
  private PlaylistRepository playlistRepository;

  @Test
  public void createPlaylistShouldReturnCreatedPlaylist() throws Exception {
    Playlist request = new Playlist("user1", "Test playlist", new ArrayList<>());
    ResponseEntity<Playlist> response = postWithAuth("/playlists", request, Playlist.class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isNotNull();
    assertThat(response.getBody().getId()).isNotNull();
    assertThat(response.getBody().getName()).isEqualTo(request.getName());
    assertThat(response.getBody().getOwner()).isEqualTo(request.getOwner());
    Playlist persistedPlaylist = playlistRepository.findById(response.getBody().getId()).orElse(null);
    assertThat(persistedPlaylist).isNotNull();
    assertThat(persistedPlaylist.getName()).isEqualTo(request.getName());
    assertThat(persistedPlaylist.getOwner()).isEqualTo(request.getOwner());
  }

  @Test
  public void createPlaylistWithoutAuthenticationShouldReturnError() throws Exception {
    Playlist request = new Playlist("user1", "Test playlist", new ArrayList<>());
    ResponseEntity<Playlist> response = postWithoutAuth("/playlists", request, Playlist.class);
    assertThat(response).isNotNull();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
  }

}
