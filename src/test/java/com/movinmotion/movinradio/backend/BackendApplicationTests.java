package com.movinmotion.movinradio.backend;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.movinmotion.movinradio.backend.controllers.AuthenticationController;
import com.movinmotion.movinradio.backend.controllers.PlaylistController;
import com.movinmotion.movinradio.backend.controllers.TrackController;

@SpringBootTest
class BackendApplicationTests {

  @Autowired
  private AuthenticationController authenticationController;

  @Autowired
  private PlaylistController playlistController;

  @Autowired
  private TrackController trackController;

  @Test
  void contextLoads() {
    assertThat(authenticationController).isNotNull();
    assertThat(playlistController).isNotNull();
    assertThat(trackController).isNotNull();
  }

}
